def condicionais():
    num_1 = 3
    num_2 = 3
    igualdade = (num_1 == num_2)
    identidade = (num_1 is num_2)
    print(igualdade)
    print(identidade)
    x = [1, 2, 3]
    y = [1, 2, 3]
    print(x == y)
    print(x is y)
    if (3 < 5) == True: # operador de igualdade
        print("Não faça isso")
    if (3 < 5) is True: # operador de identidade
        print("Não faça isso")
    if (3 < 5): # por padrão o resultado será TRUE
        print("Faça isso")
    A = []
    B = [1, 2]
    C = [3, 4]
    if not A:
        print(f'if {A}')
    elif B: 
        print(f'elif {B}')
    elif C: 
        print(f'elif {C}') 
    else:
        print(f'else {A}')
    print("final")

def controle_break():
    paises = ['Brasil', 'Portugal', 'Cabo Verde', 'Macau']
    for pais in paises:
        print(paises[-1])
        print(paises[-2])
        if pais == 'Brasil':
            print("Capital é Brasilia") 
            # break 
            # o <BREAK> PARA a função atual mas continua, ou seja, o print fim é executado
            # return
            #o <RETURN> para toda a função e não continua, ou seja, o print fim NÃO é executado
            # continue 
            # o <CONTINUE> faz com que enquanto ele entender como condicional ele vai pro próximo
        elif pais == 'Portugal':
            print("Capital é Lisboa")
    print("Fim!")

def main():
    # exemplo = condicionais()
    exemplo_dois = controle_break()

if __name__ == "__main__":
    main()