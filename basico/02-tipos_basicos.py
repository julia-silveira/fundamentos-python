# Comentários 

"""
Docstrings
"""

# Funções built-in

print("Olá Mundo!")
print(type("Olá Mundo!"))
# input()

# Funções criadas pelos usuários

def nome_funcao():
    pass

# Variáveis 

cumprimento = "Olá mundo!" # string
print(cumprimento, type(cumprimento))
cumprimento2 = f'Esta é um f-string que contem a variável cumprimento: {cumprimento}' # f-string
print(cumprimento2) 

# Exemplo de como utilizar string e f-string (EXERCÍCIO):

numero_x = "7"
numero_y = 2
numero_w = 3
somar = int(numero_x) + int(numero_y) + numero_w
subtrair = int(numero_x) - numero_y - numero_w
multiplicar = int(numero_x) * numero_y * numero_w
print(f'A soma de X + Y + W é igual a {somar}')
print(f'A subtração de X - Y - W é igual a {subtrair}')
print(f'A multiplicação de X * Y * W é igual a {multiplicar}')

# Métodos e Strings 
# Os métodos são funções dentro de uma classe. Nesse momento, o mais importante é saber que os
# métodos realizam operações em determinados tipos de dados

lista_caracteres = "Uma string pode ser uma lista de caracteres. É uma série ordenada e imutável."
substrings = lista_caracteres.split()
print(substrings)
for elemento in substrings: 
    print(elemento)
    print(f'O elemento desta lista é "{elemento}" e seu índice é "{substrings.index(elemento)}"')
    print(lista_caracteres.upper())
    print(lista_caracteres.lower())
    print(lista_caracteres.swapcase())
    print(lista_caracteres.replace("Uma", " "))
    print(lista_caracteres.replace("uma", " "))