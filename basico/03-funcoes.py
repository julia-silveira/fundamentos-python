# Funções built-in
# São funções que o Python oferece. Alguns exemplos: 
'''
print(), type(), input(), float()
'''

# Funções criadas or nós
# As funções podem ter PARAMENTROS e receber ARGUMENTOS
# Alguns exemplos: 
'''
def nome_funcao():
    print("estrutura de função")

def funcao_vazia():
    pass
'''

def nome_funcao(num_x, num_y, num_w):
    print(f'{num_x} e {num_y}. {num_w}')

nome_funcao("19/10/2021", num_w="Olá.", num_y="8h00")

# EXERCÍCIO: fazer duas funções (além de criar uma de multiplicação e uma de subtração) e anotar uma f-string