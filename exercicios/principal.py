class SobrePaises :
    def __init__(self, nome) : 
        nome = nome.lower()
        self.nome = nome

    def capital (self):
        if self.nome == 'brasil':
            return f'A capital do {self.nome.title()} é Brasilia'
        elif self.nome == 'alemanha':
            return f'A capital da {self.nome.title()} é Berlim'
        elif self.nome == 'egito':
            return f'A capital do {self.nome.title()} é Cairo'
        elif self.nome == 'china':
            return f'A capital da {self.nome.title()} é Pequim'
        else:
            return "País não encontrado na lista"

    def continente (self):
        if self.nome == 'brasil':
            return f'{self.nome.title()} faz parte do continente Americano'
        elif self.nome == 'alemanha':
            return f'{self.nome.title()} faz parte do continente Europeu'
        elif self.nome == 'egito':
            return f'{self.nome.title()} faz parte do continente Africano'
        elif self.nome == 'china':
            return f'{self.nome.title()} faz parte do continente Asiático'
        else:
            return "País não encontrado na lista"

    def idioma (self):
        if self.nome == 'brasil':
            return f'O idioma oficial do {self.nome.title()} é Português'
        if self.nome == 'alemanha':
            return f'O idioma oficial da {self.nome.title()} é Alemão'
        if self.nome == 'egito':
            return f'O idioma oficial do {self.nome.title()} é Árabe'
        if self.nome == 'china':
            return f'O idioma oficial da {self.nome.title()} é Mandarim'

class SobreCidades (SobrePaises): 
    def __init__(self, nome_cidade, nome):
        nome_cidade = nome_cidade.lower()
        super().__init__(nome)
        self.nome_cidade = nome_cidade

    def qtd_hab (self):
        if ((self.nome_cidade == 'franca') and (self.nome == 'brasil')) or (self.nome == '1'):
            return (f'{self.nome_cidade} tem 350 mil habitantes\n'
            f'Esta cidade está localizada no país {self.nome}\n'
            f'{self.capital()}\n'
            f'{self.continente()}\n'
            f'{self.idioma()}')
        elif (self.nome_cidade == 'frankfurt') and (self.nome == 'alemanha'):
            return (f'{self.nome_cidade} tem 753 mil habitantes\n'
            f'Esta cidade está localizada no país {self.nome}\n'
            f'{self.capital()}\n'
            f'{self.continente()}\n'
            f'{self.idioma()}')
        elif (self.nome_cidade == 'alexandria') and (self.nome == 'egito'):
            return (f'{self.nome_cidade} tem 5,2 milhões de habitantes\n'
            f'Esta cidade está localizada no país {self.nome}\n'
            f'{self.capital()}\n'
            f'{self.continente()}\n'
            f'{self.idioma()}')
        elif (self.nome_cidade == 'xangai') and (self.nome == 'china'):
            return (f'{self.nome_cidade} tem 26,3 milhões de habitantes\n'
            f'Esta cidade está localizada no país {self.nome}\n'
            f'{self.capital()}\n'
            f'{self.continente()}\n'
            f'{self.idioma()}')
        else:
            return "Essa cidade não faz parte da lista ou a combinação de país e cidade não tem correspondente"

def main ():
    while True: 
        lista_paises = ['Brasil', 'Alemanha', 'Egito', 'China']
        lista_cidades = ['Franca', 'Frankfurt', 'Alexandria', 'Xangai']
        """print("Escolha um dos países abaixo")
        for index, paises in enumerate(lista_paises, start=1):
            print(f'{index} - {paises}')
        pais = input("Insira o nome do país: ")
        print("Escolha a cidade correspondente ao país escolhido")
        for index, cidades in enumerate(lista_cidades, start=1):
            print(f'{index} - {cidades}')
        cidade = input("Insira o nome da cidade: ")

        cidades = SobreCidades(cidade, pais)
        print(cidades.qtd_hab())
        """
        print("Escolha uma das combinações abaixo:")
        for index, dados in enumerate(zip(lista_paises, lista_cidades), start=1):
            print(f'{index} - {dados}')
        #resultado = input("Insira a sua escolha aqui: ")

        #cidades = SobreCidades(resultado, dados)
        #print(cidades.qtd_hab())

# TAREFAS DO DIA 19/11/2021:

# utilizar ZIP para passar cidade + pais junto 
# lista_cidades + lista_paises 
# printar a resposta final 

# utilizar continue e break 

if __name__ == "__main__" :
    main()