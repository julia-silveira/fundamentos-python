def func_lambda(x,y): # função já existente no python 
    multiplicar = lambda x,y : x*y # programação funcional (simplifica)
    return multiplicar(x,y) 

def multiplicar(x,y):
    return x*y

def func_map(x):
    numeros = ['1', '2', '3', '4', '5']
    multiplicar = tuple(map(lambda x: x*2, numeros)) # map: realiza funções dentro dele 
    transformar = list(map(str, numeros)) # percorre a lista e converte seus elementos
    # return multiplicar, transformar
    for index, numero in enumerate(numeros):
        numero = str(numero)
        print(index, numero)
        numeros[index] = numero
    print(numeros)

def main():
    resultado = func_lambda(3,4)
    resultado_dois = multiplicar(8,9)
    resultado_tres = func_map(5)
    # print(resultado)
    # print(resultado_dois)
    print(resultado_tres) 

if __name__ == "__main__":
    main()