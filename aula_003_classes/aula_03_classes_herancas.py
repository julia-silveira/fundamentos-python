class Paises:
    def __init__ (self, x, y = "NA", w = "NA"):
        self.pais = x
        self.capital = y
        self.idioma  = w

    def nome(self):
        nome = self.pais
        return(nome)

    def cidade(self):
        cidade = self.capital
        return(cidade)

    def lingua(self):
        lingua = self.idioma
        return(lingua)

class Cidade(Paises):
    def __init__(self, nome, cidade, lingua, nome_da_cidade):
        super().__init__(nome, cidade, lingua)
        self.nome_cidade = nome_da_cidade 

    def qtd_habitantes(self):
        if self.nome_cidade == "Franca":
            return(f'{self.nome_cidade} tem 350 mil habitantes'\
            f'Essa cidade está localizada no país {self.nome}'\
            f'A capital desse país é: {self.cidade}'\
            f'O idioma é: {self.lingua}')
        elif self.nome_cidade == "Mendoza":
            return(f'{self.nome_cidade} tem 150 mil habitantes')

def main():
    habitantes = Cidade("Franca")
    print(habitantes.qtd_habitantes())

if __name__ == "__main__":
    main()