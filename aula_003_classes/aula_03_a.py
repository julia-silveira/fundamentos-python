# print(type("str"))
# print(dir(str))
class CalculadoraSimples: 
    def __init__ (self, x, y):
        self.num_x = x
        self.num_y = y

    def somar(self):
        soma = self.num_x + self.num_y
        return soma 

    def multiplicar(self):
        multiplicacao = self.num_x * self.num_y
        return multiplicacao
    
    def subtrair(self):
        subtrair = self.num_x - self.num_y
        return subtrair
