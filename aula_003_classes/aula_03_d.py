from aula_03_c import Paises

dados = Paises("Brasil", "Brasilia", "Português")
print(f'O nome do país é {dados.nomear()}')
print(f'A capital do {dados.nomear()} é {dados.cidade()}')
print(f'A capital do {dados.nomear()} é {dados.cidade()} e o idioma oficial é {dados.lingua()}')

resultado = Paises("Brasil", "Brasilia", "Português")
print(resultado.nomear())
print(resultado.cidade())
print(resultado.lingua())