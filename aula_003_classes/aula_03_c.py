class Paises:
    def __init__ (self, x, y = "NA", w = "NA"):
        self.pais = x
        self.capital = y
        self.idioma  = w

    def nomear(self):
        nome = self.pais
        return(nome)

    def cidade(self):
        cidade = self.capital
        return(cidade)

    def lingua(self):
        lingua = self.idioma
        return(lingua)

