def paises():
    paises = ['Brasil', 'Portugal', 'Angola', 'Macau']
    paises_dois = ['Cabo Verde']
    print(len(paises))
    paises.append('Moçambique')
    print(paises)
    paises.extend(paises_dois)
    print(paises)
    print(paises.index('Angola'))
    # print(paises.pop())
    # del paises[2]
    # print(paises.clear())
    paises_tres = paises.copy()
    paises_quatro = paises
    paises.append('Guiné Bissau')
    ordenar = paises.sort()
    print(paises)
    reverter = paises.reverse()
    print(paises)
    print(f'Os países quatro são: {paises_quatro}')
    print(f'Os países três são: {paises_tres}')
    paises[0] = 'Angola'

def main():
    resultado = paises()

if __name__ == "__main__":
    main()