def dados_paises ():
    paises_lusofonos = ['Brasil', 'Portugal', 'Angola', 'Macau']
    paises_espanhol = ['Argentina', 'Espanha', 'Uruguai', 'Paraguai']
    paises_ingles = ['Estados Unidos', 'Inglaterra', 'Australia', 'África do Sul']
    for nome in paises_lusofonos: 
        print(nome)
        print(type(nome))
    for index, item in enumerate(paises_lusofonos, start=10):
        print(f'O {item} tem o indice {index}')
    for paises in zip(paises_lusofonos, paises_espanhol, paises_ingles):
        print(paises)
        lista_paises = list(paises)
        lista_paises.append('China')
        print(lista_paises)
        print(paises.count('Brasil'))
        buscar = 'Brasil' in paises 
        print(buscar)
    for numero in range(0, 11, 2):
        print(numero)


def main ():
    paises = dados_paises()

if __name__ == "__main__" :
    main()